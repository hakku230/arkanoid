import { Container, Graphics, Text } from "pixi.js";

export class StartArea extends Container {
    constructor(width: number, height: number) {
        super()

        const bg = new Graphics()
        bg.beginFill(0x000000, 0.5)
        bg.drawRect(0, 0, width, height)
        bg.endFill()

        this.interactive = false
        
        const text = new Text('TAP ON SREEN', {fill: 0xFFFFFF, fontSize: 36})
        text.anchor.set(0.5)
        text.position.set(width / 2, height / 2)

        this.addChild(bg, text)
    }
}