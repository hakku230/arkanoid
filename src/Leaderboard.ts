import { Container, Text } from "pixi.js";

export class Leaderboard extends Container {
    header: Text
    names: Text
    points: Text
    data: object

    constructor() {
        super()

        this.header = new Text('Leaderboard', {fill: 0x000000, fontSize: 26, fontWeight: 'bold'})
        

        this.names = new Text('\n', {fill: 0x000000, fontSize: 22, align: 'left'})
        this.names.position.set(0, 40)

        this.points = new Text('\n', {fill: 0x000000, fontSize: 22, align: 'right'})
        this.points.anchor.set(1, 0)
        this.points.position.set(280, 40)

        this.data = JSON.parse(localStorage.getItem('leaderboard') || '{}')

        this.show()

        this.addChild(this.header, this.names, this.points)

        this.header.position.set(this.width / 2 - this.header.width / 2, 0)
    }

    show() {
        this.names.text = ''
        this.points.text = ''

        Object.entries(this.data).sort((a, b) => b[1] - a[1]).slice(0, 5).forEach(record => {
            this.names.text += `${record[0]}\n`
            this.points.text += `${record[1]}\n`
        })
    }

    update(name: string, points: number) {
        if ( this.data[name as keyof object] && this.data[name as keyof object] >= points ) return;

        Object.assign(this.data, {[name]: points})
        //this.data[name] = points
        localStorage.setItem('leaderboard', JSON.stringify(this.data))

        this.show()
    }
}