import { Container, Graphics } from "pixi.js"

const BALL_RADIUS = 8

export class Ball extends Container {
    velocity: {x: number, y: number} = {x: 0, y: -1}

    constructor() {
        super()

        const body = new Graphics()
        body.beginFill(0xAAAAAA)
        body.drawCircle(0, BALL_RADIUS, BALL_RADIUS)
        body.endFill()
        body.beginFill(0x888888)
        body.drawCircle(0, BALL_RADIUS, BALL_RADIUS - 1)
        body.endFill()

        this.addChild(body)
    }
}