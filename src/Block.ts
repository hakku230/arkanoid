import { Container, Graphics } from "pixi.js"

export const BLOCK_WIDTH = 64
export const BLOCK_HEIGHT = 32
const BLOCK_BORDER_SIZE = 1
const BLOCK_BORDER_COLOR = 0x888888

const BLOCK_TYPE = [
    {color: 0x0000FF, points: 1},
    {color: 0xFF0000, points: 2},
    {color: 0x00FF00, points: 3},
]

export class Block extends Container {
    color: number
    points: number

    constructor() {
        super()

        const type = BLOCK_TYPE[Math.floor(Math.random() * 3)]
        this.color = type.color
        this.points = type.points

        const body = new Graphics()
        body.beginFill(BLOCK_BORDER_COLOR)
        body.drawRect(0, 0, BLOCK_WIDTH, BLOCK_HEIGHT)
        body.endFill()
        body.beginFill(this.color)
        body.drawRect(BLOCK_BORDER_SIZE, BLOCK_BORDER_SIZE, BLOCK_WIDTH - BLOCK_BORDER_SIZE * 2, BLOCK_HEIGHT - BLOCK_BORDER_SIZE * 2)
        body.endFill()

        this.addChild(body)
    }
}