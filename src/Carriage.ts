import { Container, Graphics } from "pixi.js"

const CARRIAGE_HEIGHT = 16
const CARRIAGE_WIDTH = 96

export class Carriage extends Container {
    constructor() {
        super()

        const body = new Graphics()
        body.beginFill(0xAAAAAA)
        body.drawRoundedRect(0, 0, CARRIAGE_WIDTH, CARRIAGE_HEIGHT, 8)
        body.endFill()
        body.beginFill(0x888888)
        body.drawRoundedRect(1, 1, CARRIAGE_WIDTH - 2, CARRIAGE_HEIGHT - 2, 8)
        body.endFill()

        this.addChild(body)
    }
}