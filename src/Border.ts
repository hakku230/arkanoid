import { Container, Graphics } from "pixi.js";

export const BORDER_SIZE = 8
const BORDER_COLOR = 0xAAAAAA

export class Border extends Container {
    constructor(x: number, y: number, width: number, height: number) {
        super()

        const body = new Graphics()
        body.beginFill(BORDER_COLOR)
        body.drawRect(x, y, width, height)
        body.endFill()

        this.addChild(body)
    }
}