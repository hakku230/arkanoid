import { Container, Graphics, Text } from "pixi.js";

const INPUT_WIDTH = 240
const INPUT_HEIGHT = 50

const HIDDEN_INPUT = document.getElementById("hidden-input") as HTMLInputElement

export class Input extends Container {
    bg: Graphics
    text: Text
    disabled: boolean = false

    constructor() {
        super()

        this.bg = new Graphics()
        this.bg.beginFill(0x888888)
        this.bg.drawRect(0, 0, INPUT_WIDTH, INPUT_HEIGHT)
        this.bg.endFill()
        this.bg.beginFill(0xFFFFFF)
        this.bg.drawRect(2, 2, INPUT_WIDTH - 4, INPUT_HEIGHT - 4)
        this.bg.endFill()

        this.text = new Text('', {fill: 0x000000, fontSize: 24})
        this.text.anchor.set(0.5, 0.5)

        this.addChild(this.bg, this.text)

        this.text.position.set(INPUT_WIDTH / 2, INPUT_HEIGHT / 2)

        this.interactive = true
        this.onpointerdown = () => {
            HIDDEN_INPUT.click()
            setTimeout(function(){
                HIDDEN_INPUT.focus()
            }, 100)
        }

        HIDDEN_INPUT.onkeyup = () => {
            if (this.disabled) {
                HIDDEN_INPUT.value = this.text.text

                return;
            }

            if (HIDDEN_INPUT.value.length <= 12) this.text.text = HIDDEN_INPUT.value
            else HIDDEN_INPUT.value = this.text.text

            this.validate()
        }
    }

    enable() {
        this.disabled = false
    }

    disable() {
        this.disabled = true
    }

    get value() {
        return this.text.text
    }

    redraw(isError: boolean = false) {
        this.bg.clear()
        this.bg.beginFill(isError ? 0xFF0000 : 0x888888)
        this.bg.drawRect(0, 0, INPUT_WIDTH, INPUT_HEIGHT)
        this.bg.endFill()
        this.bg.beginFill(0xFFFFFF)
        this.bg.drawRect(2, 2, INPUT_WIDTH - 4, INPUT_HEIGHT - 4)
        this.bg.endFill()
    }

    validate() {
        if (this.value.length === 0) {
            this.redraw(true)

            return false
        }

        this.redraw(false)

        return true
    }
}