import { Container, DisplayObject } from 'pixi.js'
import { BORDER_SIZE, Border } from './Border'
import { Ball } from './Ball'
import { BLOCK_HEIGHT, BLOCK_WIDTH, Block } from './Block'
import { Carriage } from './Carriage'
import { StartArea } from './StartArea'

export const FIELD_WIDTH = BLOCK_WIDTH * 8 + BORDER_SIZE * 2
export const FIELD_HEIGHT = 768
const GAP = 96

export class Field extends Container {
    ball: Ball
    blocks: Block[] = []
    upper_border: Border
    left_border: Border
    right_border: Border
    carriage: Carriage
    startArea: Container<DisplayObject>

    mouseX: number = 0
    pause: boolean = true
    carraigeBlock: boolean = true
    points: number = 0

    constructor() {
        super()

        this.upper_border = new Border(0, 0, FIELD_WIDTH, BORDER_SIZE)
        this.left_border = new Border(0, 0, BORDER_SIZE, FIELD_HEIGHT)
        this.right_border = new Border(FIELD_WIDTH - BORDER_SIZE, 0, BORDER_SIZE, FIELD_HEIGHT)

        this.spawn()

        this.carriage = new Carriage()
        this.carriage.position.set((FIELD_WIDTH - this.carriage.width) / 2, FIELD_HEIGHT - GAP)

        this.ball = new Ball()
        this.resetBallPosition()

        this.addChild(this.upper_border , this.left_border, this.right_border, this.carriage, this.ball)

        this.startArea = new StartArea(this.width, this.height)
        this.startArea.onpointertap = () => {
            this.pause = false
            this.interactive = false
            this.startArea.visible = false
        }

        this.addChild(this.startArea)
    }

    spawn() {
        this.blocks.forEach(block => block.destroy())

        this.blocks = []

        for(let row = 0; row < 5; row++) {
            for(let blockId = 0; blockId < 8; blockId++) {
                const block = new Block()
                block.position.set(BORDER_SIZE + BLOCK_WIDTH * blockId, GAP + BLOCK_HEIGHT * row)
                this.addChild(block)
                this.blocks.push(block)
            }
        }
    }

    ready() {
        this.carraigeBlock = false
        this.startArea.interactive = true
        this.points = 0
        this.spawn()
    }

    endGame() {
        this.pause = true
        this.carraigeBlock = true

        this.startArea.visible = true
        this.startArea.interactive = false

        this.carriage.position.set((BLOCK_WIDTH * 8 + BORDER_SIZE * 2 - this.carriage.width) / 2, FIELD_HEIGHT - GAP)
        this.resetBallPosition()

        window.dispatchEvent(new CustomEvent("END_GAME", { detail: {points: this.points} }))
    }

    process(delta: number) {
        if ( !this.carraigeBlock ) {
            const CARRIAGE_SPEED = delta * 10
            
            this.moveCarriage(CARRIAGE_SPEED)

            if (this.pause) this.resetBallPosition()
        }

        if (this.pause) return;

        const BALL_SPEED = 8 * delta

        this.moveBall(BALL_SPEED)

        this.ballToCarriage(BALL_SPEED)
        
        this.ballToWalls(BALL_SPEED)

        this.ballToBlocks(BALL_SPEED)

        if (this.blocks.length === 0 || this.ball.y > FIELD_HEIGHT) this.endGame()
    }

    resetBallPosition() {
        this.ball.position.set(this.carriage.x + this.carriage.width / 2, this.carriage.y - this.carriage.height)
    }

    moveCarriage(carriageSpeed: number) {
        const carriageDestination = Math.max(BORDER_SIZE, Math.min(this.mouseX - this.carriage.width / 2, BLOCK_WIDTH * 8 + BORDER_SIZE - this.carriage.width))
        const carriageDelta = Math.abs(carriageDestination - this.carriage.x)
        this.carriage.x += Math.sign(carriageDestination - this.carriage.x) * Math.min(carriageDelta, carriageSpeed)
    }

    moveBall(ballSpeed: number) {
        this.ball.x += this.ball.velocity.x * ballSpeed
        this.ball.y += this.ball.velocity.y * ballSpeed
    }

    ballToCarriage(ballSpeed: number) {
        if ( collision(this.carriage, this.ball) ) {
            this.ball.velocity.x = (this.ball.x - this.carriage.x) / this.carriage.width * 2 - 1
            this.ball.velocity.y *= -1
            this.ball.y -= ballSpeed
        }
    }

    ballToWalls(ballSpeed: number) {
        if ( collision(this.upper_border, this.ball) ) {
            this.ball.velocity.y *= -1
            this.ball.y += ballSpeed
        }
        if ( collision(this.left_border, this.ball) ) {
            this.ball.velocity.x *= -1
            this.ball.x += ballSpeed
        }
        if ( collision(this.right_border, this.ball) ) {
            this.ball.velocity.x *= -1
            this.ball.x -= ballSpeed
        }
    }

    ballToBlocks(ballSpeed: number) {
        this.blocks.forEach(block => {
            if ( collision(block, this.ball) ) {

                const isAbove = (this.ball.y + this.ball.height) <= (block.y + ballSpeed)
                const isUnder = this.ball.y >= (block.y + block.height - ballSpeed)
                const isLeft = (this.ball.x + this.ball.width / 2) <= (block.x + ballSpeed)
                const isRight = (this.ball.x - this.ball.width / 2) >= (block.x - ballSpeed)

                if ( isAbove ) {
                    this.ball.velocity.y *= -1
                    this.ball.y -= ballSpeed
                } else if ( isUnder ) {
                    this.ball.velocity.y *= -1
                    this.ball.y += ballSpeed
                } else if ( isLeft ) {
                    this.ball.velocity.x *= -1
                    this.ball.x -= ballSpeed
                } else if ( isRight ) {
                    this.ball.velocity.x *= -1
                    this.ball.x += ballSpeed
                }

                this.points += block.points

                block.destroy()
            }
        })

        this.blocks = this.blocks.filter(block => block.destroyed ? undefined : block)
    }
}

function collision(object1: DisplayObject, object2: DisplayObject) {
    const bounds1 = object1.getBounds();
    const bounds2 = object2.getBounds();

    return bounds1.x < bounds2.x + bounds2.width
        && bounds1.x + bounds1.width > bounds2.x
        && bounds1.y < bounds2.y + bounds2.height
        && bounds1.y + bounds1.height > bounds2.y;
}