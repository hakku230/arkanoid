import { Container, DisplayObject, Graphics, Text } from "pixi.js"
import { Leaderboard } from "./Leaderboard"
import { Input } from "./Input"
import { Button } from "./Button"

export class Menu extends Container {
    button: Container<DisplayObject>
    name: string = ''
    leaderboard: Leaderboard
    input: Input
    constructor() {
        super()

        const bg = new Graphics()
        bg.beginFill(0x888888)
        bg.drawRect(0, 0, 320, 400)
        bg.endFill()
        bg.beginFill(0xAAAAAA)
        bg.drawRect(2, 2, 320 - 4, 400 - 4)
        bg.endFill()

        this.leaderboard = new Leaderboard()
        this.leaderboard.position.set(20, 20)

        const text = new Text('Enter your name:', {fill: 0x000000, fontSize: 22})

        this.input = new Input()

        this.button = new Button()

        this.addChild(bg, text, this.input, this.leaderboard, this.button)

        text.position.set(this.width / 2 - text.width / 2, 210)
        this.input.position.set(this.width / 2 - this.input.width / 2, 240)
        this.button.position.set(this.width / 2 - this.button.width / 2, 320)

        window.addEventListener('END_GAME', ((event: CustomEvent) => {
            this.leaderboard.update(this.name, event.detail.points)
            this.visible = true
            this.input.enable()
        }) as EventListener)
    }

    start() {
        this.visible = false
        this.name = this.input.value
        this.input.disable()
    }
}