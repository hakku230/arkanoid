import { Application } from 'pixi.js'
import './style.css'
import { Menu } from './Menu'
import { FIELD_HEIGHT, FIELD_WIDTH, Field } from './Field'

const app = new Application<HTMLCanvasElement>({resizeTo: window})

document.body.appendChild(app.view)

class Game {
    constructor() {
        const field = new Field()
        const menu = new Menu()
        menu.position.set(field.width / 2 - menu.width / 2, field.height / 2 - menu.height / 2)

        menu.button.onpointertap = () => {
            if ( menu.input.validate() ) {
                menu.start()
                field.ready()
            }
        }

        app.stage.addChild(field, menu)
        app.ticker.add(field.process.bind(field))

        window.onpointermove = (e) => {
            field.mouseX = (e.x - app.stage.x) / app.stage.scale.x
        }
    }

    resize() {
        const scale = Math.min(window.innerWidth / FIELD_WIDTH, window.innerHeight / FIELD_HEIGHT)
        app.stage.scale.set(scale)
        app.stage.position.set(window.innerWidth / 2 - FIELD_WIDTH * scale / 2,  window.innerHeight / 2 - FIELD_HEIGHT * scale / 2)
    }
}

const game = new Game()

window.onresize = game.resize
game.resize()