import { Container, Graphics, Text } from "pixi.js";

export class Button extends Container {
    constructor() {
        super()

        const bg = new Graphics()
        bg.beginFill(0x888888)
        bg.drawRect(0, 0, 150, 50)
        bg.endFill()
        bg.beginFill(0x555555)
        bg.drawRect(2, 2, 150 - 4, 50 - 4)
        bg.endFill()

        const text = new Text('START', {fill: 0xFFFFFF, fontSize: 24, fontWeight: 'bold'})
        text.position.set(bg.width / 2 - text.width / 2, bg.height / 2 - text.height / 2)

        this.addChild(bg, text)

        this.interactive = true
        this.cursor = 'pointer'
    }
}